const fs = require('fs');
const pug = require('pug');

const html = pug.renderFile('src/index.pug', {});

fs.writeFile('public/index.html', html, err => {
	if (err) {
		console.error(err);
	}
});