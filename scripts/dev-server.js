const express = require('express');
const pug = require('pug');

const app = express();
app.use(express.static('.'));

const port = 80;
app.listen(port, () => console.log(`Starting dev-server on port ${port}...`));

app.get('/', (req, res) => res.send(
		pug.renderFile('src/index.pug')
	)
)