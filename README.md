# Summary

My personal homepage (Ryan's IP): www.ryansip.com

# Scripts

The development scripts are executed in rootless containers run with `podman`.

The development containers keep node artifacts transparently contained while bind mounting development folders and files for internal or external modification.

## Development Server

`./scripts/dev`

```
...
# Running development server: ryansip-dev-server ...
...
[nodemon] starting `node scripts/dev-server.js`
Starting dev-server on port 80...
```

## Build/Render

`./scripts/dev -b`

```
...
# Build complete (public/) ...
index.html
static
```

## Environment Shell

`./scripts/dev -s`

```
...
# Running development shell: ryansip-dev-shell ...
ryansip-dev-shell
root@d85426cba6c4:~/homepage#
```